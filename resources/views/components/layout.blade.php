<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}" class="h-100">

    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="/css/app.css">
        <title>{{$title ?? "Welcome to BeiDenti"}}</title>
        {{$style ?? ''}}
    </head>
    
    <body>

        <div class="cover-container {{$bg ?? 'bg-img-custom d-flex'}} align-items-center w-100 vh-100 mx-auto flex-column">
            
            <x-includes.navbar/>

            {{$slot}}

        </div> 

        <x-includes.footer/>

        <script src="/js/app.js"></script>
        @stack('script')

    </body>

</html>
