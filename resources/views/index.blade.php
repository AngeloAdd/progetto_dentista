<x-layout>

  <main class="px-3 w-50 h-100 d-flex justify-content-center align-items-contente flex-column">
      <h1 class="main-text pt-4">Clinica BeiDentoni</h1>
      <p class="lead text-paragraph-custom">L'unica clinica che ti fa dei bei dentoni con poco. Dentiere, dentoni, dentini. Quello che vuoi a 100 euro.</p>
      <p class="lead">
        <a href="{{route('about')}}" class="btn btn-home-custom shadow rounded-pill fw-bold text-white px-3 py-2">Learn more</a>
      </p>
  </main>

</x-layout>